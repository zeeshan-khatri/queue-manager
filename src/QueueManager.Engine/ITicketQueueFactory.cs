﻿using System.Collections.Generic;
using QueueManager.Model;

namespace QueueManager.Engine
{
    public interface ITicketQueueFactory
    {
        Queue<Ticket> Get(Job job);
    }
}