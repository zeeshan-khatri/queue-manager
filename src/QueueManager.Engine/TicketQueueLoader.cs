using System.Collections.Generic;
using System.Linq;
using QueueManager.Model;
using QueueManager.Service;

namespace QueueManager.Engine
{
    public class TicketQueueLoader : ITicketQueueLoader
    {        
        private readonly ITicketQueueFactory _factory;
        private readonly ITicketService _ticketService;
        private readonly IJobService _jobService;

        public TicketQueueLoader(ITicketService ticketService, IJobService jobService, ITicketQueueFactory factory)
        {
            _factory = factory;
            _ticketService = ticketService;
            _jobService = jobService;
        }
                
        public void Load()
        {
            var jobs = _jobService.GetAllActive();
            
            foreach (var job in jobs)
            {
                var queue = _factory.Get(job);
                var tickets = _ticketService.GetOpenTickets(job);
                foreach (var ticket in tickets)
                {
                    queue.Enqueue(ticket);
                }
            }
        }
    }
}