﻿using Autofac;

namespace QueueManager.Engine
{
    public class EngineModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<TicketQueueFactory>().As<ITicketQueueFactory>().SingleInstance();
            builder.RegisterType<TicketQueueLoader>().As<ITicketQueueLoader>();
            builder.RegisterType<TicketEngine>().As<ITicketEngine>().SingleInstance();


            //Bind<ITicketQueueFactory>().To<TicketQueueFactory>().InSingletonScope();
            //Bind<ITicketQueueLoader>().To<TicketQueueLoader>();
            //Bind<ITicketEngine>().To<TicketEngine>().InSingletonScope();
        }
    }
}
