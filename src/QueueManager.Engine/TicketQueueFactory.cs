﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QueueManager.Model;

namespace QueueManager.Engine
{
    public class TicketQueueFactory : ITicketQueueFactory
    {
        private Dictionary<Guid, Queue<Ticket>> _queues = new Dictionary<Guid, Queue<Ticket>>();

        public Queue<Ticket> Get(Job job)
        {
            if (_queues.ContainsKey(job.Id))
                return _queues[job.Id];

            var queue = new Queue<Ticket>();
            _queues.Add(job.Id, queue);

            return queue;
        }
    }
}
