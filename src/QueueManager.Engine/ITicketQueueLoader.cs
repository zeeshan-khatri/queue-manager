﻿namespace QueueManager.Engine
{
    public interface ITicketQueueLoader
    {
        void Load();
    }
}