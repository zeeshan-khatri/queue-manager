﻿using System;
using System.Collections.Generic;
using QueueManager.Model;
using QueueManager.Service;

namespace QueueManager.Engine
{
    public enum TicketEngineState
    {
        Started,
        Stopped
    }

    public interface ITicketEngine
    {
        bool Start();
        bool Stop();

        Batch Batch { get; }        

        Ticket Enqueue(Issuer issuer, Job job);
        Ticket Dequeue(Agent agent);

        TicketEngineState State { get; }
        int Count { get; }

        Batch NewBatch();
    }

    public class TicketEngine : ITicketEngine
    {
        private readonly ITicketQueueLoader _loader;
        private readonly ITicketQueueFactory _factory;
        private readonly ITicketService _service;
        private readonly IJobService _jobService;
        private readonly IBatchService _batchService;

        private TicketEngineState _state;
        private int _count;

        public TicketEngineState State { get { return _state; } }
        public int Count { get { return _count;  } }

        public Batch NewBatch()
        {
            if (_state == TicketEngineState.Started) throw new Exception("Ticket Engine is started.");

            var batch = _batchService.StartBatch(new Batch()
            {
                Id = Guid.NewGuid(),
                From = DateTime.Now,
                NextTicketNumber = 1,
            });

            _batch = batch;
            return batch;
        }

        public TicketEngine(ITicketQueueLoader loader, ITicketQueueFactory factory, ITicketService service, IJobService jobService, IBatchService batchService)
        {
            _loader = loader;
            _factory = factory;
            _service = service;
            _jobService = jobService;
            _batchService = batchService;
 
            _state = TicketEngineState.Stopped;
        }

        public bool Start()
        {
            _loader.Load();
            _state = TicketEngineState.Started;
            return true;
        }

        public bool Stop()
        {
            _state = TicketEngineState.Stopped;
            return true;
        }

        public Batch _batch;        

        public Batch Batch
        {
            get
            {
                if(_batch != null)
                    return _batch;

                _batch = _batchService.GetCurrentBatch();
                return _batch;
            }
        }

        public Ticket Enqueue(Issuer issuer, Job job)
        {
            Validate();

            var queue = _factory.Get(job);
            
            var ticket = _service.Add(Batch, job, issuer);            
            queue.Enqueue(ticket);
            _count++;

            return ticket;
        }

        public Ticket Dequeue(Agent agent)
        {
            Validate();

            var queue = GetPeekJobQueue(agent);
            if (queue == null) return null;

            var ticket = queue.Dequeue();            
            _service.Close(ticket);
            _count--;
            return ticket;
        }

        private void Validate()
        {
            if (_state != TicketEngineState.Started) throw new Exception("Ticket Engine is not started.");
            if (Batch == null) throw new Exception("Batch is not started.");
        }
        
        private Queue<Ticket> GetPeekJobQueue(Agent agent)
        {
            var jobs = _jobService.GetByAgent(agent);
            var minimum = 0;
            Queue<Ticket> peek = null;

            foreach (var each in jobs)
            {
                var queue = _factory.Get(each);
                if(queue.Count == 0) continue;

                var ticket = queue.Peek();
                if (ticket.Number >= minimum && minimum > 0) continue;

                peek = queue;
                minimum = ticket.Number;
            }

            return peek;
        }
    }
}