﻿using System;

namespace QueueManager.Core.Data
{
    public interface IObjectContext : IDisposable
    {
        void SaveChanges();
    }
}