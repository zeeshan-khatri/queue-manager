﻿using Afterthought;
using QueueManager.Core.Data.Extensions;

namespace QueueManager.Core.Data.Amenders
{
    public class ArchivableAmender<T> : Amendment<T, T>
    {

        public ArchivableAmender()
        {
            Implement<IArchivable>();
        }
    }
}