﻿using Afterthought;
using QueueManager.Core.Data.Extensions;

namespace QueueManager.Core.Data.Amenders
{
    public class AuditableAmender<T> : Amendment<T, T>
    {

        public AuditableAmender()
        {
            Implement<IArchivable>();
        }
    }
}