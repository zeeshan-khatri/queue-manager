﻿namespace QueueManager.Core.Data
{
    public interface IUnitOfWork
    {
        void Commit();
    }
}