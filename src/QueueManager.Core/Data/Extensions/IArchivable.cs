﻿using System;

namespace QueueManager.Core.Data.Extensions
{
    public interface IArchivable
    {
        string DeletedBy { get; set; }
        DateTime? Deleted { get; set; }
    }
}
