﻿using System;

namespace QueueManager.Core.Data.Extensions
{
    public class ArchivableAttribute : Attribute { }
}
