﻿using System.Collections.Generic;

namespace QueueManager.Model
{
    public class Job : Entity
    {
        public string Code { get; set; }
        public string Title { get; set; }
        public bool IsActive { get; set; }

        public ICollection<Ticket> Tickets { get; set; }
        public ICollection<State> States { get; set; }


    }
}