﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QueueManager.Model
{
    public class State : Entity
    {
        public string Title { get; set; }
        public int Sort { get; set; }
        public Job Job { get; set; }
        public Guid JobId { get; set; }
    }
}
