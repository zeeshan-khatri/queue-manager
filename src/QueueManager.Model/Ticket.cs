﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QueueManager.Model
{
    public class Ticket : Entity
    {
        public int Number { get; set; }
        public DateTime Opening { get; set; }
        public DateTime? Closing { get; set; }

        public Job Job { get; set; }
        public State State { get; set; }
        public Batch Batch { get; set; }
        public Issuer Issuer { get; set; }

        public Guid BatchId { get; set; }
        public Guid JobId { get; set; }
        public Guid IssuerId { get; set; }
        public Guid? StateId { get; set; }
    }
}
