﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace QueueManager.Model
{
    public class Agent : Entity
    {     
        public string Code { get; set; }
        public string Title { get; set; }
        public bool IsActive { get; set; }

        public virtual ICollection<Screen> Screens { get; set; }
        public virtual ICollection<AgentJob> AgentJobs { get; set; }
        
    }

    public class AgentJob : Entity
    {
        public Guid AgentId { get; set; }
        public virtual Agent Agent { get; set; }
        public virtual Job Job { get; set; }
        public bool IsActive { get; set; }
        public Guid JobId { get; set; }
    }
}