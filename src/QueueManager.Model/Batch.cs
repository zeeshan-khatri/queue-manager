﻿using System;
using System.Collections.Generic;

namespace QueueManager.Model
{
    public class Batch : Entity
    {
        public int Number { get; set; }
        public DateTime From { get; set; }
        public DateTime? To { get; set; }
        public int NextTicketNumber { get; set; }
        public ICollection<Ticket> Tickets { get; set; }
        public bool IsActive { get; set; }
    }
}