﻿using QueueManager.Core.Data.Extensions;

namespace QueueManager.Model
{
    [Auditable, Archivable]
    public class User :Entity
    {
        public string Name { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}