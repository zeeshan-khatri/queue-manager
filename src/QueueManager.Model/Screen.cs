﻿using System;

namespace QueueManager.Model
{
    public class Screen : Entity
    {
        public Guid AgentId { get; set; }
        public virtual Agent Agent { get; set; }

        public string Title { get; set; }
        public string Address { get; set; }
    }
}