﻿using System.Collections.Generic;

namespace QueueManager.Model
{
    public class Printer : Entity
    {
        public string Title { get; set; }
        public string Address { get; set; }
        public ICollection<IssuerPrinter> Issuers { get; set; }
    }
}