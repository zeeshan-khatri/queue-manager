﻿using System;
using System.Collections.Generic;

namespace QueueManager.Model
{
    public class Issuer : Entity
    {
        public string Code { get; set; }
        public string Title { get; set; }
        public bool IsActive { get; set; }

        public ICollection<Ticket> Tickets { get; set; }
        public ICollection<IssuerPrinter> Printers { get; set; }
    }

    public class IssuerPrinter : Entity
    {
        public Issuer Issuer { get; set; }
        public Printer Printer { get; set; }
        public Guid IssuerId { get; set; }
        public Guid PrinterId { get; set; }
    }
}