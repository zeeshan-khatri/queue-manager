﻿using Ninject;
using Ninject.Modules;
using QueueManager.Data;
using QueueManager.Engine;
using QueueManager.Service;

namespace QueueManager.Test
{
    public class TestKernel
    {
        private static string toLock = "toLock";
        private static IKernel kernel = null;
   
        public static IKernel GetStandarKernel()
        {
            lock (toLock)
            {
                if (kernel != null)
                    return kernel;                

                //kernel = new StandardKernel(new INinjectModule[]{new EFModule(),new ServiceModule(), new EngineModule(), });
                return kernel;                
            }
        }
    }
}