﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ninject;
using QueueManager.Data;
using QueueManager.Model;

namespace QueueManager.Test
{
    [TestClass]
    public class JobUnitTest
    {
        private IKernel kernel;

        [TestInitialize]
        public void JobUnitTestInitialize()
        {
            kernel = TestKernel.GetStandarKernel();
        }

        [TestMethod]
        public void JobDeleteAll()
        {
            //Todo: Uncomment code after finalization
            //var uow = kernel.Get<IAppUow>();
            //uow.Jobs.Clean();
        }

        [TestMethod]
        public void JobInsertTest()
        {
            try
            {
                var uow = kernel.Get<IAppUow>();
                var job = new Job() { Id = Guid.NewGuid(), Title = "Job 1", IsActive = true};
                uow.Jobs.Add(job);
                job = new Job() { Id = Guid.NewGuid(), Title = "Job 2", IsActive = true };
                uow.Jobs.Add(job);
                job = new Job() { Id = Guid.NewGuid(), Title = "Job 3", IsActive = true };
                uow.Jobs.Add(job);
                job = new Job() { Id = Guid.NewGuid(), Title = "Job 4", IsActive = true };
                uow.Jobs.Add(job);  
                uow.Commit();

                var job2 = uow.Jobs.GetById(job.Id);
                Assert.AreEqual(job, job2);
            }
            catch (Exception e)
            {
                Assert.Fail("Job Insert Failed. Exception: {0}", e.Message);                
            }
        }

        [TestMethod]
        public void JobUpdateTest()
        {
            try
            {                
                var uow = kernel.Get<IAppUow>();
                var job = uow.Jobs.GetAll().FirstOrDefault();

                var title = string.Format("{0} - {1}", job.Title, "Updated");
                job.Title = title;
                
                uow.Jobs.Update(job);
                uow.Commit();

                var job2 = uow.Jobs.GetById(job.Id);
                Assert.AreEqual(job2.Title, title);
            }
            catch (Exception e)
            {
                Assert.Fail("Job Update Failed. Exception: {0}", e.Message);
            }
        }
    }
}
