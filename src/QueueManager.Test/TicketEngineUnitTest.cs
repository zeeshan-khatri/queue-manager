﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ninject;
using QueueManager.Data;
using QueueManager.Engine;
using QueueManager.Model;
using QueueManager.Service;

namespace QueueManager.Test
{
    [TestClass]
    public class TicketEngineUnitTest
    {
        private IKernel kernel;

        [TestInitialize]
        public void TicketEngineUnitTestInitialize()
        {
            kernel = TestKernel.GetStandarKernel();
        }

        [TestMethod]
        public void CleanTickets()
        {
            //Todo: Uncomment code after finalization
            //var uow = kernel.Get<IAppUow>();
            //uow.Tickets.Clean();
            //uow.Batches.Clean();
        }

        [TestMethod]
        public void NewBatch()
        {
            var engine = kernel.Get<ITicketEngine>();
            engine.NewBatch();
        }


        [TestMethod]
        public void EngineStart()
        {
            var engine = kernel.Get<ITicketEngine>();
            Assert.IsTrue(engine.Start());
        }

        [TestMethod]
        public void EnqueueTicket()
        {
            var engine = kernel.Get<ITicketEngine>();
            var jobs = kernel.Get<IJobService>().GetAllActive();
            var issuers = kernel.Get<IIssuerService>().GetAllActive();

            var sequence = 1;
            foreach (var job in jobs)
            {
                foreach (var issuer in issuers)
                {
                    
                    try
                    {
                        var ticket = engine.Enqueue(issuer, job);
                        Assert.IsFalse(engine.State == TicketEngineState.Stopped);
                        Assert.IsNotNull(ticket);
                        Assert.AreEqual(sequence, ticket.Number);
                        sequence++;

                    }
                    catch (Exception e)
                    {                        
                        Assert.Fail(e.Message);                        
                    }                   
                }                
            }            
        }

        [TestMethod]
        public void DequeueTickets()
        {
            var engine = kernel.Get<ITicketEngine>();
            var service = kernel.Get<IAgentService>(); 
            var agents = service.GetAllActive();

            var count = engine.Count;
            var dequeued = 0; 

            while (engine.Count > 0)
            {
                foreach (var agent in agents)
                {
                    var ticket = engine.Dequeue(agent);
                    if (ticket != null)
                    {
                        Assert.IsTrue(service.HasJob(agent, ticket.Job));
                        dequeued++;
                    }                    
                }
            }

            Assert.IsTrue(count == dequeued);
        }

        [TestMethod]
        public void EngineStop()
        {
            var engine = kernel.Get<ITicketEngine>();
            Assert.IsTrue(engine.Stop());
        }

    }


}
