﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ninject;
using QueueManager.Data;
using QueueManager.Model;

namespace QueueManager.Test
{
    [TestClass]
    public class IssuerUnitTest
    {
        private IKernel kernel;

        [TestInitialize]
        public void IssuerUnitTestInitialize()
        {
            kernel = TestKernel.GetStandarKernel();
        }

        [TestMethod]
        public void IssuerDeleteAll()
        {
            //Todo: Uncomment code after finalization
            //var uow = kernel.Get<IAppUow>();
            //uow.Issuers.Clean();
        }

        [TestMethod]
        public void IssuerInsertTest()
        {
            try
            {
                var uow = kernel.Get<IAppUow>();
                var issuer = new Issuer() { Id = Guid.NewGuid(), Code = "A001", Title = "Issuer 1", IsActive = true};
                uow.Issuers.Add(issuer);
                issuer = new Issuer() { Id = Guid.NewGuid(), Code = "A002", Title = "Issuer 2", IsActive = true };
                uow.Issuers.Add(issuer);
                issuer = new Issuer() { Id = Guid.NewGuid(), Code = "A003", Title = "Issuer 3", IsActive = true };
                uow.Issuers.Add(issuer);
                uow.Commit();

                var issuer2 = uow.Issuers.GetById(issuer.Id);
                Assert.AreEqual(issuer, issuer2);
            }
            catch (Exception e)
            {
                Assert.Fail("Issuer Insert Failed. Exception: {0}", e.Message);                
            }
        }

        [TestMethod]
        public void IssuerUpdateTest()
        {
            try
            {                
                var uow = kernel.Get<IAppUow>();
                var issuer = uow.Issuers.GetAll().FirstOrDefault();

                var title = string.Format("{0} - {1}", issuer.Title, "Updated");
                issuer.Title = title;
                
                uow.Issuers.Update(issuer);
                uow.Commit();

                var issuer2 = uow.Issuers.GetById(issuer.Id);
                Assert.AreEqual(issuer2.Title, title);
            }
            catch (Exception e)
            {
                Assert.Fail("Issuer Update Failed. Exception: {0}", e.Message);
            }
        }
    }
}
