﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ninject;
using QueueManager.Data;
using QueueManager.Model;

namespace QueueManager.Test
{
    [TestClass]
    public class AgentUnitTest
    {
        private IKernel kernel;

        [TestInitialize]
        public void AgentUnitTestInitialize()
        {
            kernel = TestKernel.GetStandarKernel();
        }

        [TestMethod]
        public void AgentDeleteAll()
        {
            var uow = kernel.Get<IAppUow>();
            uow.Agents.Clean();
        }

        [TestMethod]
        public void AgentInsertTest()
        {
            try
            {
                var uow = kernel.Get<IAppUow>();
                var jobs = uow.Jobs.GetAll().ToList();

                var agent = new Agent() { Id = Guid.NewGuid(), Code = "A001", IsActive = true, Title = "Agent 1", AgentJobs = new Collection<AgentJob>()};
                agent.AgentJobs.Add(new AgentJob() { Id = Guid.NewGuid(), Agent = agent, Job = jobs[0], IsActive = true});
                agent.AgentJobs.Add(new AgentJob() { Id = Guid.NewGuid(), Agent = agent, Job = jobs[1], IsActive = true });
                uow.Agents.Add(agent);

                agent = new Agent() { Id = Guid.NewGuid(), Code = "A002", Title = "Agent 2", IsActive = true, AgentJobs = new Collection<AgentJob>() };
                agent.AgentJobs.Add(new AgentJob() { Id = Guid.NewGuid(), Agent = agent, Job = jobs[1], IsActive = true });
                agent.AgentJobs.Add(new AgentJob() { Id = Guid.NewGuid(), Agent = agent, Job = jobs[2], IsActive = true });
                uow.Agents.Add(agent);

                agent = new Agent() { Id = Guid.NewGuid(), Code = "A003", Title = "Agent 3", IsActive = true, AgentJobs = new Collection<AgentJob>() };
                agent.AgentJobs.Add(new AgentJob() { Id = Guid.NewGuid(), Agent = agent, Job = jobs[2], IsActive = true });
                agent.AgentJobs.Add(new AgentJob() { Id = Guid.NewGuid(), Agent = agent, Job = jobs[3], IsActive = true });

                uow.Agents.Add(agent);
                uow.Commit();

                var agent2 = uow.Agents.GetById(agent.Id);
                Assert.AreEqual(agent, agent2);
            }
            catch (Exception e)
            {
                Assert.Fail("Agent Insert Failed. Exception: {0}", e.Message);                
            }
        }

        [TestMethod]
        public void AgentUpdateTest()
        {
            try
            {                
                var uow = kernel.Get<IAppUow>();
                var agent = uow.Agents.GetAll().FirstOrDefault();

                var title = string.Format("{0} - {1}", agent.Title, "Updated");
                agent.Title = title;
                
                uow.Agents.Update(agent);
                uow.Commit();

                var agent2 = uow.Agents.GetById(agent.Id);
                Assert.AreEqual(agent2.Title, title);
            }
            catch (Exception e)
            {
                Assert.Fail("Agent Update Failed. Exception: {0}", e.Message);
            }
        }
    }
}
