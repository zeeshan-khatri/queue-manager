﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QueueManager.Api.Models
{
    public class ApiResponse<T>
    {
        public ApiResponseType Type { get; set; }
        public string[] Messages { get; set; }
        public string Url { get; set; }
        public T Result { get; set; }
    }

    public enum ApiResponseType
    {
        Success = 0,
        Error = 1,
        Warning = 2
    }

    public class ApiResponseHelper
    {
        public static ApiResponse<T> Success<T>(params string[] messages) where T : class
        {
            return Success<T>(null, messages);
        }

        public static ApiResponse<T> Success<T>(T result, params string[] messages) where T : class
        {
            return Success<T>(result, null, messages);
        }

        public static ApiResponse<T> Success<T>(T result, Uri uri, params string[] messages) where T : class
        {
            return Response<T>(result, messages, uri, ApiResponseType.Success);        
        }

        public static ApiResponse<T> Error<T>(params string[] messages) where T : class
        {
            return Error<T>(null, messages);
        }

        public static ApiResponse<T> Error<T>(T result, params string[] messages) where T : class
        {
            return Error<T>(result, null, messages);
        }

        public static ApiResponse<T> Error<T>(T result, Uri uri, params string[] messages) where T : class
        {
            return Response<T>(result, messages, uri, ApiResponseType.Error);
        }


        public static ApiResponse<T> Warning<T>(params string[] messages) where T : class
        {
            return Warning<T>(null, messages);
        }

        public static ApiResponse<T> Warning<T>(T result, params string[] messages) where T : class
        {
            return Warning<T>(result, null, messages);
        }

        public static ApiResponse<T> Warning<T>(T result, Uri uri, params string[] messages) where T : class
        {
            return Response<T>(result, messages, uri, ApiResponseType.Warning);
        }


        private static ApiResponse<T> Response<T>(T result, string[] messages, Uri uri, ApiResponseType type) where T : class
        {
            return new ApiResponse<T>()
            {
                Type = type,
                Result = result,
                Messages = messages,
                Url = uri != null ? uri.ToString() : null
            };

        }


    }
}