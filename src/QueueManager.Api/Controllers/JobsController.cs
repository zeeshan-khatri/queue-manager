﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using QueueManager.Api.Models;
using QueueManager.Model;
using QueueManager.Service;

namespace QueueManager.Api.Controllers
{
    public class JobsController : BaseApiController
    {
        private const string entityName = "Job";
        protected override string EntityName { get { return entityName; } }

        public IJobService Service { get; set; }

        public JobsController(IJobService service)
        {
            Service = service;
        }

        // GET api/jobs
        public ApiResponse<IEnumerable<Job>> Get()
        {
            return GetAll(Service.GetAllActive);
        }

        // GET api/jobs/{GUID}
        public ApiResponse<Job> Get(Guid id)
        {    
            return Get(Service.Get, id);
        }

        // POST api/jobs
        public ApiResponse<Job> Post(JobVM viewModel)
        {
            var job = new Job {Code = viewModel.Code, Title = viewModel.Title};
            return Add(Service.Add, job, string.Format("{0} added successfully!", viewModel.Title));
        }

        // PUT api/jobs/{GUID}
        public ApiResponse<Job> Put(Guid id, JobVM viewModel)
        {
            var job = new Job { Id=id, Code = viewModel.Code, Title = viewModel.Title };
            return Update(Service.Update, job, string.Format("{0} updated successfully!", viewModel.Title));
        }

        // DELETE api/jobs/{GUID}
        public ApiResponse<Job> Delete(Guid id)
        {
            var job = Service.Get(id);
            return Delete(Service.Delete, job, string.Format("{0} deleted successfully!", job.Title));
        }  
    }

    public class JobVM
    {
        public string Code { get; set; }
        public string Title { get; set; }
    }
}
