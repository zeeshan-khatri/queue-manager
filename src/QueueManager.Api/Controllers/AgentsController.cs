﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using QueueManager.Api.Models;
using QueueManager.Model;
using QueueManager.Service;

namespace QueueManager.Api.Controllers
{
    public class AgentsController : BaseApiController
    {
        private const string entityName = "Agent";
        protected override string EntityName { get { return entityName; } }

        public IAgentService Service { get; set; }

        public AgentsController(IAgentService service)
        {
            Service = service;
        }

        // GET api/agents
        public ApiResponse<IEnumerable<Agent>> Get()
        {
            return GetAll(Service.GetAllActive);
        }

        // GET api/agents/{GUID}
        public ApiResponse<Agent> Get(Guid id)
        {    
            return Get(Service.Get, id);
        }

        // POST api/agents
        public ApiResponse<Agent> Post(AgentVM viewModel)
        {
            var agent = new Agent {Code = viewModel.Code, Title = viewModel.Title};
            return Add(Service.Add, agent, string.Format("{0} added successfully!", viewModel.Title));
        }

        // PUT api/agents/{GUID}
        public ApiResponse<Agent> Put(Guid id, AgentVM viewModel)
        {
            var agent = new Agent { Id=id, Code = viewModel.Code, Title = viewModel.Title };
            return Update(Service.Update, agent, string.Format("{0} updated successfully!", viewModel.Title));
        }

        // DELETE api/agents/{GUID}
        public ApiResponse<Agent> Delete(Guid id)
        {
            var agent = Service.Get(id);
            return Delete(Service.Delete, agent, string.Format("{0} deleted successfully!", agent.Title));
        }  
    }

    public class AgentVM
    {
        public string Code { get; set; }
        public string Title { get; set; }
    }
}
