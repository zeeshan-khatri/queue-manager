using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using QueueManager.Api.Models;
using QueueManager.Service.Exceptions;

namespace QueueManager.Api.Controllers
{
    public abstract class BaseApiController : ApiController
    {
        protected abstract string EntityName { get; }

        protected ApiResponse<IEnumerable<T>> GetAll<T>(Func<IEnumerable<T>> getAll, string successMessage = null) where T : class
        {
            ApiResponse<IEnumerable<T>> response;
            try
            {
                var result = getAll();
                if (result.Any())
                   response = successMessage == null ? ApiResponseHelper.Success(result) : ApiResponseHelper.Success(result, successMessage);
                else
                    response = ApiResponseHelper.Warning<IEnumerable<T>>(string.Format("No {0} found!", EntityName));
            }
            catch (Exception)
            {
                response = ApiResponseHelper.Error<IEnumerable<T>>(string.Format("Error: GetAll {0}", EntityName));
            }

            return response;
        }

        protected ApiResponse<T> Get<T>(Func<Guid, T> get, Guid id, string successMessage = null) where T : class
        {
            ApiResponse<T> response;
            try
            {
                var result = get(id);
                if(result != null)
                    response = successMessage == null ? ApiResponseHelper.Success(result) : ApiResponseHelper.Success(result, successMessage);
                else
                    response = ApiResponseHelper.Warning<T>(string.Format("No {0} found!", EntityName));
            }
            catch (Exception)
            {
                response = ApiResponseHelper.Error<T>(string.Format("Error: Get {0}", EntityName));
            }

            return response;
        }

        protected ApiResponse<T> Add<T>(Func<T, T> add, T entity, string successMessage = null) where T : class
        {
            ApiResponse<T> response;
            try
            {
                response = successMessage == null ? ApiResponseHelper.Success(add(entity)) : ApiResponseHelper.Success(add(entity), successMessage);
            }
            catch (ServiceException e)
            {
                response = ApiResponseHelper.Error<T>(e.Messages);
            }
            catch (Exception e)
            {
                response = ApiResponseHelper.Error<T>(string.Format("Error: Add {0}", EntityName));
            }

            return response;
        }


        protected ApiResponse<T> Update<T>(Func<T, T> update, T entity, string successMessage = null) where T : class
        {
            ApiResponse<T> response;
            try
            {
                response = successMessage == null ? ApiResponseHelper.Success(update(entity)) : ApiResponseHelper.Success(update(entity), successMessage);
            }
            catch (ServiceException e)
            {
                response = ApiResponseHelper.Error<T>(e.Messages);
            }
            catch (Exception)
            {
                response = ApiResponseHelper.Error<T>(string.Format("Error: Update {0}", EntityName));
            }

            return response;
        }

        protected ApiResponse<T> Delete<T>(Func<T, bool> delete, T entity, string successMessage = null) where T : class
        {
            ApiResponse<T> response;
            try
            {
                delete(entity);
                response = successMessage == null ? ApiResponseHelper.Success<T>() : ApiResponseHelper.Success<T>(successMessage);                
            }
            catch (ServiceException e)
            {
                response = ApiResponseHelper.Error<T>(e.Messages);
            }
            catch (Exception)
            {
                response = ApiResponseHelper.Error<T>(string.Format("Error: Delete {0}", EntityName));
            }

            return response;
        }


    }
}