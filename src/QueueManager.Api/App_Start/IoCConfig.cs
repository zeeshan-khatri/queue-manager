﻿using System.Linq;
using System.Reflection;
using System.Web.Compilation;
using System.Web.Http;
using Autofac;
using Autofac.Core;
using Autofac.Integration.WebApi;

namespace QueueManager.Api
{
    public class IocConfig
    {
        public static IContainer Container { get; set; }
        public static void Register()
        {
            // Create the container builder.
            var builder = new ContainerBuilder();

            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            var assemblies = BuildManager.GetReferencedAssemblies().Cast<Assembly>().Where(a => a.FullName.StartsWith("QueueManager"));
            builder.RegisterAssemblyModules(assemblies.ToArray());

            // Build the container.
            var container = builder.Build();

            // Create the depenedency resolver.
            var resolver = new AutofacWebApiDependencyResolver(container);

            // Configure Web API with the dependency resolver.
            GlobalConfiguration.Configuration.DependencyResolver = resolver;

            Container = container;
        }
    }
}