﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using Autofac;
using Autofac.Integration.WebApi;
using Module = Autofac.Module;

namespace QueueManager.Api.Ioc
{
    public class WebApiModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            // Register the Web API controllers.
            

            // Register other dependencies.
            //builder.Register(c => new Logger()).As<ILogger>().InstancePerApiRequest();
        }
    }
}