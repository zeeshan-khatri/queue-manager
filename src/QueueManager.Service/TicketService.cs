﻿using System;
using System.Collections.Generic;
using System.Linq;
using QueueManager.Core.Data;
using QueueManager.Model;

namespace QueueManager.Service
{
    public interface ITicketService
    {
        IEnumerable<Ticket> GetOpenTickets(Job job);

        Ticket Close(Ticket ticket);
        Ticket Get(Guid id);

        Ticket Add(Batch batch, Job job, Issuer issuer);
    }

    public class TicketService : DataService, ITicketService
    {
        
        public TicketService(IUnitOfWork uow, IRepository<Ticket> tickets, IRepository<Batch> batches)
        {
            Uow = uow;
            Tickets = tickets;
            Batches = batches;
        }

        public IRepository<Batch> Batches { get; set; }

        public IRepository<Ticket> Tickets { get; set; }

        public IEnumerable<Ticket> GetOpenTickets(Job job)
        {
            return Tickets.GetAll(t => t.JobId == job.Id && t.Batch.IsActive && t.Closing == null)
                .OrderBy(t => t.Number);
        }

        public Ticket Close(Ticket ticket)
        {
            ticket.Closing = DateTime.Now;
            Tickets.Update(ticket);
            Uow.Commit();

            return Get(ticket.Id);
        }

        public Ticket Get(Guid id)
        {
            return Tickets.First(t => t.Id == id);
        }

        public Ticket Add(Batch batch, Job job, Issuer issuer)
        {
            var ticket = new Ticket()
            {
                Id = Guid.NewGuid(),
                Number = batch.NextTicketNumber++,
                BatchId = batch.Id,
                Opening = DateTime.Now,
                JobId = job.Id,
                IssuerId = issuer.Id,
            };            
            
            Tickets.Insert(ticket);
            Batches.Update(batch);
            Uow.Commit();

            ticket = Get(ticket.Id);

            return ticket;
        }
    }
}
