﻿using System;
using System.Collections.Generic;
using System.Linq;

using QueueManager.Core.Data;
using QueueManager.Model;
using QueueManager.Service.Exceptions;

namespace QueueManager.Service
{
    public interface IJobService
    {
        IEnumerable<Job> GetAllActive();
        IEnumerable<Job> GetByAgent(Agent agent);

        Job Get(Guid id);
        Job Add(Job job);
        Job Update(Job job);
        bool Delete(Job job);
    }

    public class JobService : DataService, IJobService
    {
        public JobService(IUnitOfWork uow, IRepository<Job> jobs, IRepository<AgentJob> agentJobs)
        {
            Uow = uow;
            Jobs = jobs;
            AgentJobs = agentJobs;
        }

        public IRepository<AgentJob> AgentJobs { get; set; }

        public IRepository<Job> Jobs { get; set; }

        public IEnumerable<Job> GetAllActive()
        {
            return Jobs.GetAll().Where(j => j.IsActive).ToList();
        }

        public IEnumerable<Job> GetByAgent(Agent agent)
        {
            return AgentJobs.GetAll().Where(aj => aj.AgentId == agent.Id && aj.IsActive).Select(aj => aj.Job).ToList();
        }

        public Job Get(Guid id)
        {
            //return Uow.Jobs.GetById(id);
            return Jobs.Single(a => a.Id == id);
        }

        public Job Add(Job job)
        {
            job.Id = Guid.NewGuid();
            job.IsActive = true;

            Validate(job);

            Jobs.Insert(job);
            Uow.Commit();
            return job;
        }

        public Job Update(Job job)
        {
            var existing = Get(job.Id);
            existing.Code = job.Code;
            existing.Title = job.Title;

            Validate(job);

            Jobs.Update(existing);
            Uow.Commit();
            return existing;
        }

        public bool Delete(Job job)
        {
            Jobs.Delete(job);
            Uow.Commit();
            return true;
        }

        public void Validate(Job job)
        {
            var messages = new List<string>();

            var checkCode = Jobs.GetAll().FirstOrDefault(a => a.Code == job.Code && a.Id != job.Id);
            if (checkCode != null)
                messages.Add(string.Format(@"Job Code '{0}' already exist.", job.Code));

            var checkTitle = Jobs.GetAll().FirstOrDefault(a => a.Title == job.Title && a.Id != job.Id);
            if (checkTitle != null)
                messages.Add(string.Format(@"Job Title '{0}' already exist.", job.Title));

            if (messages.Any())
            {
                throw new ServiceException(messages.ToArray());
            }
        }
    }
}