﻿using System.Linq;
using QueueManager.Core.Data;
using QueueManager.Model;

namespace QueueManager.Service
{
    public interface IBatchService
    {
        void Update(Batch currentBatch);
        Batch GetCurrentBatch();
        Batch StartBatch(Batch batch);
    }

    public class BatchService : DataService, IBatchService
    {
        public BatchService(IUnitOfWork uow, IRepository<Batch> batches)
        {
            Uow = uow;
            Batches = batches;
        }

        public IRepository<Batch> Batches { get; set; }

        public void Update(Batch batch)
        {
            Batches.Update(batch);            
            Uow.Commit();
        }

        public Batch GetCurrentBatch()
        {
            return Batches.First(b => b.IsActive);
        }

        private int GetNextNumber()
        {
            //Todo: find out best option
            int? max = Batches.GetAll().Max(b => (int?) b.Number);
            if (max.HasValue)
                return max.Value + 1;

            return 1;
        }


        public Batch StartBatch(Batch batch)
        {
            var current = GetCurrentBatch();
            if (current != null)
            {
                current.IsActive = false;
                Batches.Update(current);
            }
                
            batch.Number = GetNextNumber();
            batch.IsActive = true;

            Batches.Insert(batch);            
            Uow.Commit();

            return batch;
        }
    }
}