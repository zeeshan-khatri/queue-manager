﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QueueManager.Service.Exceptions
{
    public class ServiceException : Exception
    {
        public ServiceException(params string[] messages)
        {
            Messages = messages;
        }

        public string[] Messages { get; private set; }
    }
}
