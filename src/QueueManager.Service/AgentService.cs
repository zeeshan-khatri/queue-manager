﻿using System;
using System.Collections.Generic;
using System.Linq;
using QueueManager.Core.Data;
using QueueManager.Model;
using QueueManager.Service.Exceptions;

namespace QueueManager.Service
{
    public interface IAgentService
    {
        IEnumerable<Agent> GetAllActive();
        bool HasJob(Agent agent, Job job);

        Agent Get(Guid id);
        Agent Add(Agent agent);
        Agent Update(Agent agent);
        bool Delete(Agent agent);
    }

    public class AgentService : DataService, IAgentService
    {
        public IRepository<AgentJob> AgentJobs { get; set; }
        public IRepository<Agent> Agents { get; set; }


        public AgentService(IUnitOfWork uow, IRepository<Agent> agents, IRepository<AgentJob> agentJobs)
        {
            Uow = uow;
            Agents = agents;
            AgentJobs = agentJobs;

        }

        public IEnumerable<Agent> GetAllActive()
        {
            //return Uow.Agents.GetAll().Where(j => j.IsActive).ToList();
            return Agents.GetAll().Where(j => j.IsActive).ToList();
        }

        public bool HasJob(Agent agent, Job job)
        {
            //return Uow.AgentJobs.GetAll()
            //    .Any(aj => aj.AgentId == agent.Id && aj.JobId == job.Id && aj.IsActive);
            return AgentJobs.First(aj => aj.AgentId == agent.Id && aj.JobId == job.Id && aj.IsActive) != null;
        }

        public Agent Get(Guid id)
        {
            //return Uow.Agents.GetById(id);
            return Agents.Single(a =>  a.Id == id);
        }

        public Agent Add(Agent agent)
        {           
            agent.Id = Guid.NewGuid();
            agent.IsActive = true;

            Validate(agent);

            Agents.Insert(agent);
            Uow.Commit();
            return agent;
        }

        public Agent Update(Agent agent)
        {            
            var existing = Get(agent.Id);
            existing.Code = agent.Code;
            existing.Title = agent.Title;

            Validate(agent);

            Agents.Update(existing);
            Uow.Commit();
            return existing;
        }

        public bool Delete(Agent agent)
        {
            Agents.Delete(agent);
            Uow.Commit();
            return true;
        }

        public void Validate(Agent agent)
        {
            var messages =  new List<string>();

            var checkCode = Agents.GetAll().FirstOrDefault(a => a.Code == agent.Code && a.Id != agent.Id);           
            if(checkCode != null)
                messages.Add(string.Format(@"Agent Code '{0}' already exist.", agent.Code));

            var checkTitle = Agents.GetAll().FirstOrDefault(a => a.Title == agent.Title && a.Id != agent.Id);
            if (checkTitle != null)
                messages.Add(string.Format(@"Agent Title '{0}' already exist.", agent.Title));

            if (messages.Any())
            {
                throw new ServiceException(messages.ToArray());
            }
        }
    }
}