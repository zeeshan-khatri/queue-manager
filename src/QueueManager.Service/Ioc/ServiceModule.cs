﻿using System.Reflection;
using Autofac;
using QueueManager.Core.Services;
using Module = Autofac.Module;

namespace QueueManager.Service
{
    public class ServiceModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var serivces = Assembly.GetExecutingAssembly();

            builder.RegisterAssemblyTypes(serivces)
                .Where(t => typeof(IService).IsAssignableFrom(t))
                .AsImplementedInterfaces();
        }
    }
}
