﻿using System.Collections.Generic;
using System.Linq;

using QueueManager.Core.Data;
using QueueManager.Model;

namespace QueueManager.Service
{
    public interface IIssuerService
    {
        IEnumerable<Issuer> GetAllActive();
    }

    public class IssuerService : DataService, IIssuerService
    {
        public IssuerService(IUnitOfWork uow, IRepository<Issuer> issuers)
        {
            Uow = uow;
            Issuers = issuers;
        }

        public IRepository<Issuer> Issuers { get; set; }

        public IEnumerable<Issuer> GetAllActive()
        {
            return Issuers.GetAll().Where(j => j.IsActive).ToList();
        }
    }
}