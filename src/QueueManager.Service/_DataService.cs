using QueueManager.Core.Data;
using QueueManager.Core.Services;

namespace QueueManager.Service
{
    public abstract class DataService : IService
    {
        protected IUnitOfWork Uow { get; set; }
    }
}