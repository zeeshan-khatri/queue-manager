﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QueueManager.Service.Helper
{
    public static class StringExtensions
    {
        const string Delimeter = "; ";
        public static string ToConcatenatedString(this List<string> list)
        {
            return list.Aggregate((i, j) => i + Delimeter + j);
        }
    }
}
