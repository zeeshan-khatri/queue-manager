﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using QueueManager.Core.Data;

namespace QueueManager.Data
{
    public class EFModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            //System.Diagnostics.Debugger.Break();

            // Registering interfaces of Unit Of Work & Generic Repository
            builder.RegisterGeneric(typeof(Repository<>)).As(typeof(IRepository<>));
            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>();

            // Every time we ask for a EF context, we'll pass our own Context.
            builder.RegisterType<AppDbContext>().As<DbContext>();

            // Tricky part.
            // Your repositories and unit of work must share the same DbContextAdapter, so we register an instance that will always be used
            // on subsequente resolve.
            // Note : you should not use ContainerControlledLifetimeManager when using ASP.NET or MVC
            // and use a per request lifetime manager
            builder.Register((context, adapter) => new DbContextAdapter(context.Resolve<DbContext>())).InstancePerRequest();

            builder.Register<IObjectSetFactory>((context, obj) => context.Resolve<DbContextAdapter>());
            builder.Register<IObjectContext>((context, obj) => context.Resolve<DbContextAdapter>());

            //Source Sample from http://efpatterns.codeplex.com/wikipage?title=Ioc%20%3a%20Unity%202.1&referringTitle=Documentation

            //// Registering interfaces of Unit Of Work & Generic Repository
            //UnityContainer.RegisterType(typeof(IRepository<>), typeof(Repository<>));
            //UnityContainer.RegisterType(typeof(IUnitOfWork), typeof(UnitOfWork));

            //// Every time we ask for a EF context, we'll pass our own Context.
            //UnityContainer.RegisterType(typeof(DbContext), typeof(Context));

            //// Tricky part.
            //// Your repositories and unit of work must share the same DbContextAdapter, so we register an instance that will always be used
            //// on subsequente resolve.
            //// Note : you should not use ContainerControlledLifetimeManager when using ASP.NET or MVC
            //// and use a per request lifetime manager
            //UnityContainer.RegisterInstance(new DbContextAdapter(UnityContainer.Resolve<DbContext>()), new ContainerControlledLifetimeManager());

            //UnityContainer.RegisterType<IObjectSetFactory>(
            //    new InjectionFactory(con => con.Resolve<DbContextAdapter>())
            //    );

            //UnityContainer.RegisterType<IObjectContext>(
            //    new InjectionFactory(con => con.Resolve<DbContextAdapter>())
            //    );
        }
    }
}
