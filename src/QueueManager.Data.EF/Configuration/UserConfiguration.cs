﻿using System.Data.Entity.ModelConfiguration;
using QueueManager.Model;

namespace QueueManager.Data.Configuration
{
    public class UserConfiguration : EntityTypeConfiguration<User>
    {
        public UserConfiguration()
        {
            Property(u => u.Name).IsRequired();
            Property(u => u.UserName).IsRequired();
            Property(u => u.Password).IsRequired();
        }
    }
}