﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QueueManager.Model;

namespace QueueManager.Data.Configuration
{
    public class IssuerConfiguration : EntityTypeConfiguration<Issuer>
    {
        public IssuerConfiguration()
        {
            Property(i => i.Code).IsRequired();
            Property(i => i.Title).IsRequired();
            HasMany(i => i.Tickets).WithRequired(t => t.Issuer).HasForeignKey(t => t.IssuerId);
            HasMany(i => i.Printers).WithRequired(p => p.Issuer).HasForeignKey(p => p.IssuerId);            
        }

        
    }
}
