﻿using System.Data.Entity.ModelConfiguration;
using QueueManager.Model;

namespace QueueManager.Data.Configuration
{
    public class PrinterConfiguration : EntityTypeConfiguration<Printer>
    {
        public PrinterConfiguration()
        {
            Property(p => p.Title).IsRequired();
            Property(p => p.Address).IsRequired();

            HasMany(p => p.Issuers).WithRequired(i => i.Printer).HasForeignKey(i => i.PrinterId);
            ;
        }
    }
}