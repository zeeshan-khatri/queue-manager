﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QueueManager.Model;

namespace QueueManager.Data.Configuration
{
    public  class AgentConfiguration : EntityTypeConfiguration<Agent>
    {
        public AgentConfiguration()
        {            
            Property(a => a.Code).IsRequired().HasMaxLength(4);
            Property(a => a.Title).IsRequired().HasMaxLength(50);
            HasMany(a => a.Screens).WithRequired(a => a.Agent).HasForeignKey(s => s.AgentId);
            HasMany(a => a.AgentJobs).WithRequired(aj => aj.Agent).HasForeignKey(aj => aj.AgentId);
            
        }
    }
}
