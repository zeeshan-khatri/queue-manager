﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using QueueManager.Model;

namespace QueueManager.Data.Configuration
{
    public class JobConfiguration : EntityTypeConfiguration<Job>
    {
        public JobConfiguration()
        {
            Property(a => a.Code).IsRequired().HasMaxLength(4);
            Property(j => j.Title).IsRequired().HasMaxLength(50);

            HasMany(j => j.Tickets).WithRequired(s => s.Job).HasForeignKey(s => s.JobId);
            HasMany(j => j.States).WithRequired(s => s.Job).HasForeignKey(s => s.JobId);

        }
    }
}