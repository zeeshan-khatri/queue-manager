using System.Data.Entity.ModelConfiguration;
using QueueManager.Model;

namespace QueueManager.Data.Configuration
{
    public class TicketConfiguration : EntityTypeConfiguration<Ticket>
    {
        public TicketConfiguration()
        {
            Property(t => t.Number).IsRequired();
            Property(t => t.Opening).IsRequired();            
        }
    }
}