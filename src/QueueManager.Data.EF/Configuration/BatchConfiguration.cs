﻿using System.Data.Entity.ModelConfiguration;
using QueueManager.Model;

namespace QueueManager.Data.Configuration
{
    public class BatchConfiguration : EntityTypeConfiguration<Batch>
    {
        public BatchConfiguration()
        {
            Property(b => b.Number).IsRequired();
            Property(b => b.NextTicketNumber).IsRequired();
            Property(b => b.From).IsRequired();
            HasMany(b => b.Tickets).WithRequired(t => t.Batch).HasForeignKey(t => t.BatchId);
        }
    }
}