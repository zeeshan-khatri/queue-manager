﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Reflection;
using QueueManager.Model;

namespace QueueManager.Data
{
    public class AppDbContext : DbContext 
    {
        // ToDo: Move Initializer to Global.asax; don't want dependence on SampleData
        static AppDbContext()
        {
            Database.SetInitializer(new AppDatabaseInitializer());            
            //Database.SetInitializer(new MigrateDatabaseToLatestVersion<AppDbContext, MigrationConfiguration>());
        }

        public AppDbContext()
            : base(nameOrConnectionString: "AppDB") { }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // Use singular table names
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();                                    
            modelBuilder.Configurations.AddFromAssembly(Assembly.GetAssembly(typeof(AppDbContext)));
            
        }

        public DbSet<Batch> Batches { get; set; }
        public DbSet<Ticket> Tickets { get; set; }
        public DbSet<Agent> Agents { get; set; }
        public DbSet<AgentJob> AgentJobs { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Job> Jobs { get; set; }
        public DbSet<Screen> Screens { get; set; }
        public DbSet<Printer> Printers { get; set; }
        public DbSet<State> States { get; set; }

    }
}