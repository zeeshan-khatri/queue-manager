﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using QueueManager.Model;

namespace QueueManager.Data
{ 
    public class AppDatabaseInitializer :        
        //CreateDatabaseIfNotExists<AppDbContext>      // when model is stable
        DropCreateDatabaseIfModelChanges<AppDbContext> // when iterating
    {

        protected override void Seed(AppDbContext context)
        {
            //// Seed code here
            //var users = AddUsers(context);
            //Save(context);

            //var agents = AddAgents(context);
            //Save(context);
        }

        private List<User> AddUsers(AppDbContext context)
        {
            //var levels = new List<Level>()
            //    {
            //        new Level(){Number = "L1", Name = "Level 1", Description = "CEO/ Managing Director level access", IsActive = true},
            //        new Level(){Number = "L2", Name = "Level 2", FolderName = "L2 - Data", Description = "Sales Manager", IsActive = true},
            //        new Level(){Number = "L3", Name = "Level 3", FolderName = "L3 - Data", Description = "Sales Representatives", IsActive = true},
            //        new Level(){Number = "L4", Name = "Level 4", FolderName = "L4 - Data", Description = "", IsActive = true},
            //        new Level(){Number = "L5", Name = "Level 5", FolderName = "L5 - Data", Description = "Technicians/ Operational Users", IsActive = true},
            //    };


            //foreach (var level in levels)
            //{
            //    context.Levels.Add(level);
            //}

            return null;
        }

        private List<Agent> AddAgents(AppDbContext context)
        {
            //var reports = new List<Report>()
            //{
            //    new Report(){Name = "stock-on-hand", DisplayName = "Stock On Hand", Level = levels.FirstOrDefault(l => l.Number == "L5"), HasDateRange = false, IsActive = true},
            //    new Report(){Name = "customer-orders", DisplayName = "Customer Orders", Level = levels.FirstOrDefault(l => l.Number == "L2"), HasDateRange = true, DaysBefore = 15, DaysAfter = 15, IsActive = true, ParentLevel = levels.FirstOrDefault(l => l.Number == "L1")},
            //    new Report(){Name = "customer-sales", DisplayName = "Customer Sales", Level = levels.FirstOrDefault(l => l.Number == "L2"), HasDateRange = true, DaysBefore = 15, DaysAfter = 15, IsActive = true, ParentLevel = levels.FirstOrDefault(l => l.Number == "L1")},
            //};


            //foreach (var report in reports)
            //{
            //    context.Reports.Add(report);
            //}
            
            return null;
        }

    }
}