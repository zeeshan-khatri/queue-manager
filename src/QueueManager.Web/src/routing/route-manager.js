﻿define(['jquery',
    'lodash',
    'angular',    
], function ($, _, angular) {

    //var name = "queue.manager.routing";

    var RouteManager = function($routeProvider) {
        $routeProvider
            .when("/login", {
                templateUrl: './src/modules/authentication/login.html',
                controller: "LoginController"
            })
            .when("/admin", {
                templateUrl: './src/modules/admin/admin.html',
                controller: "AdminController"
            })
            .when("/admin/agents", {
                templateUrl: './src/modules/admin/agent/agent.html',
                controller: "AgentController"
            })
            .when("/admin/jobs", {
                templateUrl: './src/modules/admin/job/job.html',
                controller: "JobController"
            })
            .when("/", {
                templateUrl: './src/modules/portal/home.html',
                controller: "HomeController"
            })
            .otherwise("/login");
    };

    return ["$routeProvider", RouteManager];

});