﻿define(['jquery',
    'lodash',
    'angular',
    'core/api-response-type',
], function ($, _, angular, ApiReponseType) {

    var MessageInterceptor = function ($q, $window, $rootScope) {

        return function (promise) {

            var onSuccess = function(response) {
                if (_.has(response.data, "type") && _.has(response.data,"messages") && response.data.messages.length) {
                    $rootScope.alerts.push({
                        type: ApiReponseType[response.data.type],
                        messages: response.data.messages,
                        url: response.data.url,
                        hide: false
                });
                }

                return response;
            };

            var onError = function(response) {
                return response;
            };

            return promise.then(onSuccess, onError);
        };
    };

    return ["$q","$window","$rootScope", MessageInterceptor];

});