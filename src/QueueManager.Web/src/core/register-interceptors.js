﻿define(['jquery',
    'lodash',
    'angular',    
], function ($, _, angular) {

    var RegisterInterceptor = function ($httpProvider) {

        $httpProvider.responseInterceptors.push('MessageInterceptor');  
    };

    return ["$httpProvider", RegisterInterceptor];

});