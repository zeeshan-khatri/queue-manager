﻿define([
  'jquery',
  'angular',
  'ngRoute',
  'ngResource',
  'admin/admin-module',
  'portal/portal-module',
  'auth/authentication-module',
  'util/utility-module',
  'common/common-module',
  'routing/route-manager',
  'core/register-interceptors',
  'core/message-interceptor',
  'bootstrap',
], function ($, angular, ngRoute, ngResource, AdminModule, PortalModule, AuthenticationModule, UtilityModule, CommonModule, RouteManager, RegisterInterceptors, MessageInterceptor) {

    ////setting up foundation
    //$(document).foundation({});

    //registering angular app    
    var name = "queue.manager";
    var appName = "Queue Manager";

    var app = angular
        .module(name, ["ngRoute", "ngResource", AdminModule, PortalModule, AuthenticationModule, UtilityModule, CommonModule])
        .config(RouteManager)
        .config(RegisterInterceptors)
        .factory("MessageInterceptor", MessageInterceptor)
        .run(["$rootScope", function($rootScope) {
            angular.extend($rootScope, {
                        appName: appName,
                        alerts: [],
                    });
                }
        ]);

    angular.bootstrap($('body'), [name]);

    return app;

});