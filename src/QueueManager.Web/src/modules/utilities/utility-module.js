﻿define(['jquery',
    'lodash',
    'angular',
    'util/directives/bootstrap-menu',
    'util/directives/bootstrap-alert',
    'util/directives/confirm-click',
], function ($, _, angular, BootstrapMenu, BootstrapAlert, ConfirmClick) {
    11
    var name = "queue.manager.utilities";

    angular.module(name, [])
        .directive('bootstrapMenu', BootstrapMenu)
        .directive('bootstrapAlert', BootstrapAlert)
        .directive('confirmClick', ConfirmClick)
    ;

    return name;

});