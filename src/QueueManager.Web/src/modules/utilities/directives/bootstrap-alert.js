﻿define([
], function () {

    var BootstrapAlert = function ($timeout) {
        return {
            restrict: "E",
            tranclude: true,
            templateUrl: './src/modules/utilities/directives/bootstrap-alert-template.html',
            scope: {
                alert: '=ngModel',
                close: '&onClose',
            },
            link: function (scope, element, attrs) {
                //todo: Not working
                $timeout(function () {
                    if (scope.close)
                        scope.close(scope.alert);
                }, 5000);
            }
        };
    };

    return ['$timeout', BootstrapAlert];

});