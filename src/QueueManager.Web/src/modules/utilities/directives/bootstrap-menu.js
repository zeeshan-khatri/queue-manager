﻿define(['jquery', 'lodash', 'angular'
], function ($, _, angular) {

    var BootstrapMenu = function () {
        return {
            restrict: "E",
            templateUrl: './src/modules/utilities/directives/bootstrap-menu-template.html',
            scope: {
                treeData: '=',
                onSelect: '&',
            },
            link: function(scope, element, attrs)
            {
                
            }
        };
    };

    return [BootstrapMenu];

});