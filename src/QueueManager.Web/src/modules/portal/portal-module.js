﻿define([
  'angular',
  'portal/home-controller'
], function (angular, HomeController) {

    var name = "queue.manager.portal";
    angular
        .module(name, [])
        .controller("HomeController",HomeController);

    return name;

});