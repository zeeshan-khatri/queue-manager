﻿define([
    'angular',
    'auth/login-controller',
    'auth/session-controller'
], function (angular, LoginController, SessionController) {

    var name = "queue.manager.authentication";

    angular
        .module(name, [])
        .controller("LoginController", LoginController)
        .controller("SessionController", SessionController);

    return name;

});