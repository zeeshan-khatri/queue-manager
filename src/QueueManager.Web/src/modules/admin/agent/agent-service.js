﻿define([
], function () {

    var AgentService = function ($resource) {

        var Agents = $resource("/api/agents/:Id", { 'Id': "@Id" }, { update: { method: 'PUT' } });

        return {
            getAll: function(callback) {                
                Agents.get({},function(response) {
                    if(callback) callback(response.result);
                });
            },

            add: function (agent, callback) {
               Agents.save(agent, function(response) {
                   if (callback) callback(response.result);
               });               
            },

            update: function(agent, callback) {
                Agents.update({'Id': agent.id}, agent, function(response) {
                    if (callback) callback(response.result);
                });
            },

            remove: function(agent, callback) {
                Agents.delete({ 'Id': agent.id }, function (response) {
                    if (callback) callback(response.type != 1);
                });
            }
        };
    };
    return ["$resource", AgentService];

});