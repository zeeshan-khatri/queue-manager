﻿define(["lodash"
], function (_) {

    var AgentController = function($scope, AgentService) {
        var empty = { id: "", code: "", title: "" };

        $scope.title = "Agent";
        $scope.selected = angular.copy(empty);
        $scope.master = empty;
        $scope.isEdit = false;

        $scope.hasChanged = function() {
            return !angular.equals($scope.selected, $scope.master);
        };

        $scope.clearSearch = function()
        {
            $scope.searchAgent = "";
        };

        $scope.addAgent = function () {
            AgentService.add($scope.selected, function (agent) {
                if (agent) {
                    $scope.agents.push(agent);
                    $scope.cancelAgent();
                }                    
            });             
        };

        $scope.updateAgent = function() {
            AgentService.update($scope.selected, function (agent) {
                if (agent) {
                    _.merge($scope.master, agent);
                    $scope.cancelAgent();
                }
            });
           
        };

        $scope.editAgent = function (agent) {
            $scope.master = agent;
            $scope.selected = angular.copy(agent);
            $scope.isEdit = true;
        };

        $scope.deleteAgent = function (agent) {
            AgentService.remove(agent, function(done) {
                if (done) {                    
                    _($scope.agents).remove(agent);
                    if ($scope.master == agent) {
                        $scope.cancelAgent();
                    }
                }                    
            });
        };

        $scope.cancelAgent = function () {
            $scope.selected = angular.copy(empty);
            $scope.master = empty;
            $scope.isEdit = false;
        };

        AgentService.getAll(function(result) {
            if (result)
                $scope.agents = result;
            else
                $scope.agents = [];
        });      
    };

    return ["$scope","AgentService", AgentController];

});