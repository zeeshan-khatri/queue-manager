﻿define([
  'angular',
  'admin/admin-controller',
  'admin/agent/agent-controller',
  'admin/agent/agent-service',
  'admin/job/job-controller',
  'admin/job/job-service',
], function (angular, AdminController, AgentController, AgentService, JobController, JobService) {

    var name = "queue.manager.admin";
    angular
        .module(name, ['ngResource'])
        .factory("AgentService", AgentService)
        .factory("JobService", JobService)
        .controller("AdminController", AdminController)
        .controller("AgentController", AgentController)
        .controller("JobController", JobController);
            


    return name;

});