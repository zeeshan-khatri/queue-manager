﻿define(["lodash"
], function (_) {

    var JobController = function($scope, JobService) {
        var empty = { id: "", code: "", title: "" };

        $scope.title = "Job";
        $scope.selected = angular.copy(empty);
        $scope.master = empty;
        $scope.isEdit = false;

        $scope.hasChanged = function() {
            return !angular.equals($scope.selected, $scope.master);
        };

        $scope.clearSearch = function()
        {
            $scope.searchJob = "";
        };

        $scope.addJob = function () {
            JobService.add($scope.selected, function (job) {
                if (job) {
                    $scope.jobs.push(job);
                    $scope.cancelJob();
                }                    
            });             
        };

        $scope.updateJob = function() {
            JobService.update($scope.selected, function (job) {
                if (job) {
                    _.merge($scope.master, job);
                    $scope.cancelJob();
                }
            });
           
        };

        $scope.editJob = function (job) {
            $scope.master = job;
            $scope.selected = angular.copy(job);
            $scope.isEdit = true;
        };

        $scope.deleteJob = function (job) {
            JobService.remove(job, function(done) {
                if (done) {                    
                    _($scope.jobs).remove(job);
                    if ($scope.master == job) {
                        $scope.cancelJob();
                    }
                }                    
            });
        };

        $scope.cancelJob = function () {
            $scope.selected = angular.copy(empty);
            $scope.master = empty;
            $scope.isEdit = false;
        };

        JobService.getAll(function(result) {
            if (result)
                $scope.jobs = result;
            else
                $scope.jobs = [];
        });      
    };

    return ["$scope","JobService", JobController];

});