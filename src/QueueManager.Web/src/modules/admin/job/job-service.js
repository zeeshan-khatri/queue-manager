﻿define([
], function () {

    var JobService = function ($resource) {

        var Jobs = $resource("/api/jobs/:Id", { 'Id': "@Id" }, { update: { method: 'PUT' } });

        return {
            getAll: function(callback) {                
                Jobs.get({},function(response) {
                    if(callback) callback(response.result);
                });
            },

            add: function (job, callback) {
               Jobs.save(job, function(response) {
                   if (callback) callback(response.result);
               });               
            },

            update: function(job, callback) {
                Jobs.update({'Id': job.id}, job, function(response) {
                    if (callback) callback(response.result);
                });
            },

            remove: function(job, callback) {
                Jobs.delete({ 'Id': job.id }, function (response) {
                    if (callback) callback(response.type != 1);
                });
            }
        };
    };
    return ["$resource", JobService];

});