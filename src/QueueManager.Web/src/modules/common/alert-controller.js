﻿define(['lodash'
], function (_) {

    var AlertController = function ($scope, $rootScope) {
        $scope.title = "I am AlertController";
        $scope.alerts = $rootScope.alerts;
        $scope.deleteAlert = function (alert) {
            $rootScope.alerts = _.without($rootScope.alerts, alert);
            $scope.alerts = $rootScope.alerts;
        };
        $scope.hideAlert = function (alert) {
            var result = _.find($rootScope.alerts, function (item) { return item == alert; });
            if(result)
                result.hide = true;
            $scope.alerts = $rootScope.alerts;
        };
    };

    return ["$scope", "$rootScope", AlertController];

});