﻿define([
  'angular',
  'common/menu-controller',
  'common/alert-controller',
], function (angular, MenuController, AlertController) {

    var name = "queue.manager.common";
    angular
        .module(name, [])
        .controller("MenuController", MenuController)
        .controller("AlertController", AlertController)
    ;
    return name;

});