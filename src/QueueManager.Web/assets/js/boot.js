﻿/**
 * Require config
 */
require.config({
    appDir: '',
    baseUrl: './src',
    paths: {
        /* jQuery */
        'jquery': '../lib/jquery/dist/jquery',

        /* lodash */
        "lodash": "../lib/lodash/dist/lodash",

        /* bootstrap */
        'bootstrap': '../lib/bootstrap/dist/js/bootstrap',
        
        /* angular */
        'angular': '../lib/angular/angular',
        'ngRoute': '../lib/angular-route/angular-route',
        'ngResource': '../lib/angular-resource/angular-resource',

        /* Application */
        'admin': './modules/admin',
        'auth': './modules/authentication',
        'util': './modules/utilities',
        'common': './modules/common',
        'portal': './modules/portal'
    },
    shim: {
        /* lodash */
        'lodash': {
            exports: '_',
        },
        
        /* angular */
        'angular': {
            exports: 'angular',
            deps: ['jquery']
        },

        'ngRoute' : {
            deps: ['angular']
        },

        'ngResource': {
            deps: ['angular']
        },

        'bootstrap' : {
            deps: ['jquery']
        }

    },
    deps: [
    'app'
    ]
});
